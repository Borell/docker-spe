FROM golang:latest
USER root

CMD apt-get update && apt-get install -y iputils-ping

CMD apt install -y git && apt-get install -y unzip

CMD mkdir /devops/
CMD mkdir /devops/app

WORKDIR /devops/app/
CMD chmod -R 666 /devops/app/

WORKDIR /home/web000p1/inst001/logs
WORKDIR /home/web000p1/inst001/applications/conf/emr-rrbox
WORKDIR /home/web000p1/inst001/certificats/keystore
WORKDIR /home/web000p1/inst001/applications/statics

